<?php

namespace App\Exports;

use App\Models\Calendar;
use Maatwebsite\Excel\Concerns\FromCollection;

class CalendarExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Calendar::all();
    }
}