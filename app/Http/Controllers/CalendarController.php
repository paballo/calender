<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\CalendarExport;

use DB;

use PDF;

use Excel;

use App\Models\Calendar;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $calendar = calendar::all();

        return view('calendar' , compact('calendar'));
    }

    public function pdf()
    {
      $calendar = calendar::all();
      $pdf=PDF::loadView('/calendarpdf',compact('calendar'));
      return $pdf->download('calendar.pdf');
  
  }

  public function export()
  {
    return Excel::download( new CalendarExport, 'users.xlsx');
  }

  public function sendemail(Request $request)
      {
        
         //$user = User::find($id);
         
         $email = $request->input('email');
         $details = [
        'title' => 'Geekulcha',
        'body' => 'Calendar'
        ];
   
       \Mail::to($email)->send(new \App\Mail\MyTestMail($details));
   
        dd("Email is Sent.");
      } 

}
