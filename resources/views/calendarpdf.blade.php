<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Starting Date</th>
            <th scope="col">Ending Date</th>
            <th scope="col">Title</th>
            <th scope="col">Task</th>
            <th scope="col">Needs</th>
            <th scope="col">Support</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($calendar as $item)
            <tr>
                <th scope="row">{{$item->id}}</th>
                <td>{{$item->sdate}}</td>
                <td>{{$item->edate}}</td>
                <td>{{$item->title}}</td>
                <td>{{$item->task}}</td>
                <td>{{$item->needs}}</td>
                <td>{{$item->support}}</td>
                <td>{{$item->description}}</td>
              </tr>
            @endforeach
         
          
        </tbody>
      </table>
</body>
</html>